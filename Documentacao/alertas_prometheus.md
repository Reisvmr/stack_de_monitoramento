# REFERENCIAS DE ALERTAS:
https://awesome-prometheus-alerts.grep.to/rules.html
***

# BLACKBOX ALERTAS

```yaml
  - alert: Webmonitor_Falhou
    expr: probe_success == 0
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: "Webmonitor Falhou (instance {{ $labels.instance }})"
      description: "Webmonitor Falhou\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: Webmonitor_Lento
    expr: avg_over_time(probe_duration_seconds[1m]) > 10
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: "Webmonitor Lento (instance {{ $labels.instance }})"
      description: "A sonda da caixa preta levou mais de 10s para ser concluída\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert:  Webmonitor_Http_Falhou
    expr: probe_http_status_code <= 199 OR probe_http_status_code >= 400
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: "Falha de HTTP(instance {{ $labels.instance }})"
      description: "O código de status HTTP não é 200-399\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: O_certificado_Ssl_irá_expirar_em_breve
    expr: probe_ssl_earliest_cert_expiry - time() < 86400 * 30
    for: 0m
    labels:
      severity: warning
    annotations:
      summary: "O certificado irá expirar em breve (instance {{ $labels.instance }})"
      description: "O certificado SSL expira em 30 dias\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: O_certificado_Ssl_irá_expirar_em_breve
    expr: probe_ssl_earliest_cert_expiry - time() < 86400 * 3
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: "O certificado irá expirar em breve (instance {{ $labels.instance }})"
      description: "O certificado SSL expira em 3 dias\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: Certificado_Ssl_Expirado
    expr: probe_ssl_earliest_cert_expiry - time() <= 0
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: "Certificado Ssl Expirado (instance {{ $labels.instance }})"
      description: " O certificado SSL já expirou\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: Requisicao_HTTP_lenta
    expr: avg_over_time(probe_http_duration_seconds[1m]) > 5
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: "Requisição HTTP lenta (instance {{ $labels.instance }})"
      description: "A solicitação HTTP demorou mais de 5s\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: BlackboxProbeSlowPing
    expr: avg_over_time(probe_icmp_duration_seconds[1m]) > 1
    for: 1m
    labels:
      severity: warning
    annotations:
      summary: "Blackbox probe slow ping (instance {{ $labels.instance }})"
      description: " Blackbox ping took more than 1s\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"
```
