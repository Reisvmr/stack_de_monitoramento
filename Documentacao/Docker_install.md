## REMOVE DOCKER

### Antes de fazer a instalação remova qualquer instalação anterior.

```shell

sudo apt-get purge docker-ce docker-ce-cli containerd.io

sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd

```
***
## REMOVE DOCKER-COMPOSE

```shell

sudo rm /usr/local/bin/docker-compose


pip uninstall docker-compose
```

## INSTALACAO:

1- Update e instalação de pre-rec

```shell

sudo apt-get update


sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

2- Adicione a chave GPG oficial do Docker:

```shell

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

```
3- Use o seguinte comando para configurar o repositório estável . Para adicionar o repositório noturno ou de teste , adicione a palavra nightlyou test(ou ambos) após a palavra stablenos comandos abaixo. Aprenda sobre canais noturnos e de teste .


```shell

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

```
***

# INSTALE O DOCKER ENGINE
1- Atualize o aptíndice do pacote e instale a versão mais recente do Docker Engine e containerd ou vá para a próxima etapa para instalar uma versão específica:

```shell
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io


```

2- Para instalar uma versão específica do Docker Engine, liste as versões disponíveis no repo, selecione e instale:

uma. Liste as versões disponíveis em seu repo:

```shell
 apt-cache madison docker-ce

```
 * O resultado de ser algo como abaixo

```shell

 docker-ce | 5:20.10.5~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:20.10.4~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:20.10.3~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:20.10.2~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:20.10.1~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:20.10.0~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.15~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.14~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.13~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.12~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.11~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.10~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
 docker-ce | 5:19.03.9~3-0~ubuntu-focal | https://download.docker.com/linux/ubuntu focal/stable amd64 Packages


```

 * Instale uma versão específica usando a string de versão da segunda coluna, por exemplo 5:18.09.1~3-0~ubuntu-xenial,.

```shell
 sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
```
caso contrario basta rodar o seguinte comando, e a versao mais recente sera instalada.


```shell
 sudo apt-get install docker-ce

```

3- Verifique se o Docker Engine está instalado corretamente executando a hello-world imagem.



```shell
sudo docker run hello-world

```
***
# Instale o Compose em sistemas Linux:

---
**NOTE**

No Linux, você pode baixar o binário do Docker Compose na página de lançamento do repositório do Compose no GitHub . Siga as instruções do link, que envolvem a execução do curlcomando em seu terminal para baixar os binários. Essas instruções passo a passo também estão incluídas abaixo.

---

1- Execute este comando para baixar a versão estável atual do Docker Compose:

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose


```

2- Aplique permissões executáveis ​​ao binário:

```shell
sudo chmod +x /usr/local/bin/docker-compose


```

3- Teste a instalação.


```shell
sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
```

## Gerenciar o Docker como um usuário não raiz:


### Adicione o grupo de janelas de encaixe se ele ainda não existir:

```shell
sudo groupadd docker
```
### Adicione o usuário conectado "$ USER" ao grupo de janelas de encaixe. Altere o nome do usuário para corresponder ao seu usuário preferido, se você não quiser usar seu usuário atual:

```shell
sudo gpasswd -a $USER docker
```
