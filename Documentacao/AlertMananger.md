# Alertas com Prometheus são separados em duas partes:

### Regras de alerta no servidor Prometheus que enviam para o Alertmanager;

### Alertmanager então gerencia esses alertas, incluindo silenciamento, inibição, agregação e enviando notificações através de métodos tais como: e-mail, telegram, PagerDuty e HipChat.
***

### Os principais passos para configuração de alertas e notificações são: 

* Instalar e configurar o Alertmanager;

* Configurar o Prometheus para falar com o Alertmanager;

* Criar as regras de alerta no Prometheus.













***
# CONFIGURAÇÃO

O Alertmanager é configurado por meio de sinalizadores de linha de comando e um arquivo de configuração. Enquanto os sinalizadores de linha de comando configuram parâmetros de sistema imutáveis, o arquivo de configuração define regras de inibição, roteamento de notificação e receptores de notificação.

***

Para ver todos os sinalizadores de linha de comando disponíveis, execute alertmanager -h.

O Alertmanager pode recarregar sua configuração em tempo de execução. Se a nova configuração não estiver bem formada, as alterações não serão aplicadas e um erro será registrado. Uma recarga de configuração é disparada enviando um SIGHUPpara o processo ou enviando uma solicitação HTTP POST para o /-/reloadponto de extremidade.

***
Arquivo de configuração
Para especificar qual arquivo de configuração carregar, use o  --config.file flag.

```yaml
./alertmanager --config.file=alertmanager.yml
```
O arquivo é escrito no formato YAML , definido pelo esquema descrito a seguir. Os colchetes indicam que um parâmetro é opcional. Para parâmetros não listados, o valor é definido como o padrão especificado.

Os marcadores de posição genéricos são definidos da seguinte forma:

***

* ```<duration>:``` uma duração que corresponde à expressão regular [0-9]+(ms|[smhdwy])
* ```<labelname>:``` uma string que corresponde à expressão regular [a-zA-Z_][a-zA-Z0-9_]*
* ```<labelvalue>:``` uma string de caracteres Unicode
* ```<filepath>:``` um caminho válido no diretório de trabalho atual
* ```<boolean>:``` um booleano que pode assumir os valores trueoufalse
* ```<string>:``` uma string regular
* ```<secret>:``` uma string regular que é um segredo, como uma senha
* ```<tmpl_string>:``` uma string que é expandida por modelo antes do uso
* ```<tmpl_string>:``` uma string que é expandida por modelo antes do uso
* ```<tmpl_secret>:``` uma string que é expandida por modelo antes do uso que é um segredo
* ```<int>:``` um valor inteiro

***

Os outros marcadores de posição são especificados separadamente.

Um arquivo de exemplo válido fornecido mostra o uso no contexto.
Podemos utilizar este simulador para ajudar na criacao/correcao dos arquivos .
https://www.prometheus.io/webtools/alerting/routing-tree-editor/

A configuração global especifica parâmetros que são válidos em todos os outros contextos de configuração. Eles também servem como padrões para outras seções de configuração.

```yaml
global:
  # O campo de cabeçalho SMTP 
  [ smtp_from: <tmpl_string> ]
  # O host inteligente SMTP padrão usado para enviar e-mails, incluindo o número da porta.
  # O número da porta geralmente é 25 ou 587 para SMTP sobre TLS (às vezes chamado de STARTTLS).
  # Exemplo: smtp.example.org:587
  [ smtp_smarthost: <string> ]
  # O nome de host padrão a ser identificado para o servidor SMTP.
  [ smtp_hello: <string> | default = "localhost" ]
  # Autenticação SMTP usando CRAM-MD5, LOGIN e PLAIN. Se estiver vazio, o Alertmanager não se autentica no servidor SMTP.
  [ smtp_auth_username: <string> ]
  # SMTP Auth using LOGIN and PLAIN.
  [ smtp_auth_password: <secret> ]
  # Autenticação SMTP usando LOGIN e PLAIN.
  [ smtp_auth_identity: <string> ]
  # Autenticação SMTP usando CRAM-MD5.
  [ smtp_auth_secret: <secret> ]
  # O requisito padrão de SMTP TLS.
  # Observe que Go não oferece suporte a conexões não criptografadas para pontos de extremidade SMTP remotos.
  [ smtp_require_tls: <bool> | default = true ]

  # O URL da API a ser usada para notificações do Slack.
  [ slack_api_url: <secret> ]
  [ victorops_api_key: <secret> ]
  [ victorops_api_url: <string> | default = "https://alert.victorops.com/integrations/generic/20131114/alert/" ]
  [ pagerduty_url: <string> | default = "https://events.pagerduty.com/v2/enqueue" ]
  [ opsgenie_api_key: <secret> ]
  [ opsgenie_api_url: <string> | default = "https://api.opsgenie.com/" ]
  [ wechat_api_url: <string> | default = "https://qyapi.weixin.qq.com/cgi-bin/" ]
  [ wechat_api_secret: <secret> ]
  [ wechat_api_corp_id: <string> ]
# A configuração padrão do cliente HTTP
  [ http_config: <http_config> ]
# ResolveTimeout é o valor padrão usado pelo alertmanager se o alerta
# não inclui EndsAt, após esse tempo passar, ele pode declarar o alerta como resolvido se não tiver sido atualizado.
# Isso não tem impacto sobre os alertas do Prometheus, pois eles sempre incluem EndsAt.
  [ resolve_timeout: <duration> | default = 5m ]
# Arquivos a partir dos quais as definições do modelo de notificação personalizada são lidas.
# O último componente pode usar um matcher curinga, por exemplo, 'templates / *. tmpl'.
templates:
  [ - <filepath> ... ]
# O nó raiz da árvore de roteamento.
route: <route>
# Uma lista de destinatários de notificação.
receivers:
  - <receiver> ...
# Uma lista de regras de inibição.
inhibit_rules:
  [ - <inhibit_rule> ... ]
```
***
 ## ```<route>```
***
Um bloco de rota define um nó em uma árvore de roteamento e seus filhos. Seus parâmetros de configuração opcionais são herdados de seu nó pai se não forem configurados.

Cada alerta entra na árvore de roteamento na rota de nível superior configurada, que deve corresponder a todos os alertas (ou seja, não ter correspondências configuradas). Em seguida, ele atravessa os nós filhos. Se continuefor definido como falso, ele para após o primeiro filho correspondente. Se continuefor verdadeiro em um nó correspondente, o alerta continuará correspondendo aos irmãos subsequentes. Se um alerta não corresponder a nenhum filho de um nó (nenhum nó filho correspondente ou não existir), o alerta será tratado com base nos parâmetros de configuração do nó atual.
```yaml
[ receiver: <string> ]
# Sao os rótulos pelos quais os alertas recebidos são agrupados. Por exemplo,
# alerta múltiplo entrando para cluster = A e alertname = LatencyHigh iria
# ser agrupado em um único grupo.
#
# Para agregar por todos os rótulos possíveis, use o valor especial '...' como o único nome do rótulo, por exemplo:
# group_by: ['...']
# Isso desativa efetivamente a agregação por completo, passando por todos
# alertas no estado em que se encontram. É improvável que seja o que você deseja, a menos que você tenha
# um volume de alerta muito baixo ou seu sistema de notificação upstream executa
# seu próprio agrupamento.

[ group_by: '[' <labelname>, ... ']' ]

# Se um alerta deve continuar correspondendo a nós irmãos subsequentes.
[ continue: <boolean> | default = false ]

# Um conjunto de correspondências de igualdade que um alerta deve preencher para corresponder ao nó.
match:
  [ <labelname>: <labelvalue>, ... ]

# Um conjunto de correspondências de regex que um alerta deve atender para corresponder ao nó.
match_re:
  [ <labelname>: <regex>, ... ]

# Quanto tempo esperar inicialmente para enviar uma notificação para um grupo
# de alertas. Permite esperar que um alerta de inibição chegue ou receba
# mais alertas iniciais para o mesmo grupo. (Normalmente ~ 0s a alguns minutos.)
[ group_wait: <duration> | default = 30s ]

# Quanto tempo esperar antes de enviar uma notificação sobre novos alertas que
# são adicionados a um grupo de alertas para os quais uma notificação inicial foi
# já foi enviado. (Normalmente cerca de 5 m ou mais.)

[ group_interval: <duration> | default = 5m ]

# Quanto tempo esperar antes de enviar uma notificação novamente se já tiver
# enviado com sucesso para um alerta. (Normalmente ~ 3h ou mais).
[ repeat_interval: <duration> | default = 4h ]

# Zero ou mais rotas secundárias.
routes:
  [ - <route> ... ]

```
***

## Exemplo

```yaml
  # A rota raiz com todos os parâmetros, que são herdados pelo filho
  # rotas se não forem substituídas.
route:
  receiver: 'default-receiver'
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 4h
  group_by: [cluster, alertname]
  # Todos os alertas que não correspondem às seguintes rotas secundárias
  # permanecerá no nó raiz e será despachado para 'receptor padrão'.
  routes:
  # Todos os alertas com service = mysql ou service = cassandra
  # são despachados para o pager do banco de dados.
  - receiver: 'database-pager'
    group_wait: 10s
    match_re:
      service: mysql|cassandra
  # Todos os alertas com o rótulo team = frontend correspondem a esta rota secundária.
  # Eles são agrupados por produto e ambiente, em vez de cluster
  # e alertname.
  - receiver: 'frontend-pager'
    group_by: [product, environment]
    match:
      team: frontend
```
***

## ```<http_config>```

A http_configpermite configurar o cliente HTTP que o receptor usa para se comunicar com serviços API baseados em HTTP.

```yaml
# Note that `basic_auth`, `bearer_token` and `bearer_token_file` options are
# mutually exclusive.

# Sets the `Authorization` header with the configured username and password.
# password and password_file are mutually exclusive.
basic_auth:
  [ username: <string> ]
  [ password: <secret> ]
  [ password_file: <string> ]

# Sets the `Authorization` header with the configured bearer token.
[ bearer_token: <secret> ]

# Sets the `Authorization` header with the bearer token read from the configured file.
[ bearer_token_file: <filepath> ]

# Configures the TLS settings.
tls_config:
  [ <tls_config> ]

# Optional proxy URL.
[ proxy_url: <string> ]

```
***

## ```<receiver>```

Receptor é uma configuração nomeada de uma ou mais integrações de notificação.

Novos receptores nao sao criados com frequencia,é altamente recomendado a implementação de integrações de notificação personalizadas por meio do receptor de webhook .

```yaml
# The unique name of the receiver.
name: <string>

# Configurations for several notification integrations.
email_configs:
  [ - <email_config>, ... ]
pagerduty_configs:
  [ - <pagerduty_config>, ... ]
pushover_configs:
  [ - <pushover_config>, ... ]
slack_configs:
  [ - <slack_config>, ... ]
opsgenie_configs:
  [ - <opsgenie_config>, ... ]
webhook_configs:
  [ - <webhook_config>, ... ]
victorops_configs:
  [ - <victorops_config>, ... ]
wechat_configs:
  [ - <wechat_config>, ... ]

```
*** 
## ``` <email_config>```

```yaml
# Whether or not to notify about resolved alerts.
[ send_resolved: <boolean> | default = false ]

# The email address to send notifications to.
to: <tmpl_string>

# The sender address.
[ from: <tmpl_string> | default = global.smtp_from ]

# The SMTP host through which emails are sent.
[ smarthost: <string> | default = global.smtp_smarthost ]

# The hostname to identify to the SMTP server.
[ hello: <string> | default = global.smtp_hello ]

# SMTP authentication information.
[ auth_username: <string> | default = global.smtp_auth_username ]
[ auth_password: <secret> | default = global.smtp_auth_password ]
[ auth_secret: <secret> | default = global.smtp_auth_secret ]
[ auth_identity: <string> | default = global.smtp_auth_identity ]

# The SMTP TLS requirement.
# Note that Go does not support unencrypted connections to remote SMTP endpoints.
[ require_tls: <bool> | default = global.smtp_require_tls ]

# TLS configuration.
tls_config:
  [ <tls_config> ]

# The HTML body of the email notification.
[ html: <tmpl_string> | default = '{{ template "email.default.html" . }}' ]
# The text body of the email notification.
[ text: <tmpl_string> ]

# Further headers email header key/value pairs. Overrides any headers
# previously set by the notification implementation.
[ headers: { <string>: <tmpl_string>, ... } ]

```
***
## INTEGRACOES

### Alertas do Prometheus Alertmanager with MS Teams
REFERENCIAS:
* https://github.com/prometheus-msteams/prometheus-msteams
* https://lapee79.github.io/en/article/prometheus-alertmanager-with-msteams/
  


Alertmanager suporta Email, HipChat, PagerDuty, Slack etc. nativamente, exceto Microsoft Teams. Portanto, é necessário usar o Webhook para enviar uma ferramenta de terceiros que envia mensagens ao Microsoft Teams.

Esta postagem pressupõe que você está usando o operador Prometheus para monitorar seu cluster Kubernetes. Vamos saber como enviar alertas do Prometheus para o Microsoft Teams.

**Obtenha o URL do Webhook de entrada do Microsoft Teams**

Esta tutorial pressupõe que você esteja familiarizado com os conceitos básicos do Microsoft Teams. Vamos começar a configurar o Webhook de entrada.

Vá até o canal, onde deseja receber alertas. 

01- Clique em à ...direita do nome do canal e selecione Connectors na lista suspensa.
Selecionando conector ![alt text](Documentacao/img/teams-select-connector.png "CONECTOR SELECT")

02- Selecione o Incoming Webhookconector na lista de conectores disponíveis.
Selecione o Incoming Webhookconector ![alt text](Documentacao/img/select-incomingwebhook.png "Incoming Webhookconector")


03- Insira um nome para identificar este webhook posteriormente. Você também pode adicionar uma imagem que ficará visível sempre que uma mensagem for postada usando este webhook. Clique Create.

Insirindo um nome para identificar ![alt text](Documentacao/img/naming-incommingwebhook.png "identificar webhook")

04- Isso irá gerar um URL de webhook que pode ser usado para postar mensagens neste canal. Copie este webhook e salve-o para mais tarde.

URL de webhook  ![alt text](Documentacao/img/webhook-url.png "URL de webhook")

***
### Configurar o operador Alertmanager of Prometheus

Para enviar a ferramenta de terceiros para o Microsoft Teams, gere alertmanager.yaml para configurar o Alertmanager.

```yaml
global:
  resolve_timeout: 5m
receivers:
- name: prometheus-msteams
  webhook_configs:
  - url: "http://prometheus-msteams:2000/alertmanager"
    send_resolved: true
route:
  group_by:
  - job
  group_interval: 5m
  group_wait: 30s
  receiver: prometheus-msteams
  repeat_interval: 12h
  routes:
  - match:
      alertname: Watchdog
    receiver: prometheus-msteams

```
O arquivo acima usa webhook como receptor e especifica o nome como prometheus-msteams . Em seguida, codifique o alertmanager.yaml usando base64 .
```
cat alertmanager.yaml | base64
Z2xvYmFsOgogIHJlc29sdmVfdGltZW91dDogNW0KcmVjZWl2ZXJzOgotIG5hbWU6IHByb21ldGhldXMtbXN0ZWFtcwogIHdlYmhvb2tfY29uZmlnczoKICAtIHVybDogImh0dHA6Ly9wcm9tZXRoZXVzLW1zdGVhbXM6MjAwMC9hbGVydG1hbmFnZXIiCiAgICBzZW5kX3Jlc29sdmVkOiB0cnVlCnJvdXRlOgogIGdyb3VwX2J5OgogIC0gam9iCiAgZ3JvdXBfaW50ZXJ2YWw6IDVtCiAgZ3JvdXBfd2FpdDogMzBzCiAgcmVjZWl2ZXI6IHByb21ldGhldXMtbXN0ZWFtcwogIHJlcGVhdF9pbnRlcnZhbDogMTJoCiAgcm91dGVzOgogIC0gbWF0Y2g6CiAgICAgIGFsZXJ0bmFtZTogV2F0Y2hkb2cKICAgIHJlY2VpdmVyOiBwcm9tZXRoZXVzLW1zdGVhbXMK

```
Crie um objeto Secret com a string gerada acima que é usada pelo Alertmanager do operador Prometheus e aplique-o ao Kubernetes.

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: alertmanager-main
  namespace: monitoring
type: Opaque
data:
  alertmanager.yaml: Z2xvYmFsOgogIHJlc29sdmVfdGltZW91dDogNW0KcmVjZWl2ZXJzOgotIG5hbWU6IHByb21ldGhldXMtbXN0ZWFtcwogIHdlYmhvb2tfY29uZmlnczoKICAtIHVybDogImh0dHA6Ly9wcm9tZXRoZXVzLW1zdGVhbXM6MjAwMC9hbGVydG1hbmFnZXIiCiAgICBzZW5kX3Jlc29sdmVkOiB0cnVlCnJvdXRlOgogIGdyb3VwX2J5OgogIC0gam9iCiAgZ3JvdXBfaW50ZXJ2YWw6IDVtCiAgZ3JvdXBfd2FpdDogMzBzCiAgcmVjZWl2ZXI6IHByb21ldGhldXMtbXN0ZWFtcwogIHJlcGVhdF9pbnRlcnZhbDogMTJoCiAgcm91dGVzOgogIC0gbWF0Y2g6CiAgICAgIGFsZXJ0bmFtZTogV2F0Y2hkb2cKICAgIHJlY2VpdmVyOiBwcm9tZXRoZXVzLW1zdGVhbXMK
EOF

```

Agora o Alertmanager está pronto para enviar alertas usando o webhook.

Definir externalLabels para vários clusters
Se você tiver vários clusters do Kubernetes, precisará ser capaz de idenficar qual cluster está gerando alertas. Add cluster: <CLUSTER_NAME>em externalLabels para Prometheus CRD para que os alertas tenham informações do cluster.

