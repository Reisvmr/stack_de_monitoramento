### O que é o Prometheus?

Conforme descrito no próprio github da ferramenta, o Prometheus é um sistema de monitoramento para serviços e aplicações. Ele coleta as métricas de seus alvos em determinados intervalos, avalia expressões de regras, exibe os resultados e também pode acionar alertas se alguma condição for observada como verdadeira.
  
**Dentre muitas, estas são principais características do Prometheus:**

* É um modelo de dados multi-dimensional (time series).
* Possui uma linguagem própria(PromQL) para queries de dados em formato time series.
* Totalmente autônomo, sem dependência de armazenamento externo.
* A coleta das métricas ocorre com um modelo pull e via HTTP. (Ou seja o servidor busca a metricas no agent)
* Também é possível enviar métricas através de um gateway intermediário.
* A definição dos serviços a serem monitorados pode ser feita através de uma configuração estática ou através de descoberta.
* Possui vários modos de suporte a gráficos e painéis.

***

O objetivo deste documento é focar no entendimento sobre como configurar a coleta de métricas — utilizando o modelo pull via HTTP — na sua aplicação e também a visualização delas. Sendo assim, aqui não serão abordados assuntos como configuração e implantação do Prometheus. Contudo, para ter uma ideia do funcionamento geral da ferramenta, é interessante entender como funcionam os componentes internos do Prometheus.
***
**Componentes do Prometheus**

O Prometheus possui um componente central chamado Prometheus Server. Este componente possui como responsabilidade monitorar, ele pode ser monitorar um servidor Linux, um servidor Apache HTTP, um processo específico, um banco de dados ou até uma aplicação desenvolvida por você.

Sendo assim, quando falamos de métricas, podemos estar falando de qualquer característica presente nos exemplos citados. Por exemplo, uma métrica pode ser o status da CPU, o uso de memória de um servidor, a quantidade de requisições de sua aplicação, ou qualquer outra unidade que seja possível quantificar.
***
### Em resumo, o funcionamento do Prometheus acontece da seguinte forma:

Dado um intervalo de tempo, o Prometheus Server coleta métricas de seus alvos através do protocolo HTTP. Após coletadas, as métricas são armazenadas num banco de dados time-series para posteriormente serem consultadas. Os alvos e a configuração de intervalo para coleta são definidos em um arquivo chamado "prometheus.yml" .

O Prometheus possui uma linguagem para consulta das métricas armazenadas, a **PromQL**. Utilizando esta linguagem, é possível consultar os detalhes das métricas em um dado instante da linha de tempo.

Outra característica importante é que o Prometheus disponibiliza diversas bibliotecas em várias linguagens que são utilizadas para monitorar as aplicações que desenvolvemos. Além disso, existem também os exporters, eles servem para monitorar os sistemas de terceiros (Linux, MySQL, etc). Um exporter é um software que coleta as métricas existentes de um sistema de terceiros e as exporta para o formato métrico que o Prometheus server pode entender.
***
Figura 1 — Overview do Prometheus

![alt text](Documentacao/img/Overview_do_Prometheus.png "Figura 1 — Overview do Prometheus")

## Componentes
O ecosistema do Prometheus consiste de multiplos componentes, varios são opcionais: 
* O principal Prometheus server que coleta e armazena time series data;
* client libraries para instrumentar código de aplicações; 
* Um push gateway para suportar tarefas de curta duração;
* Exportadores especiais para serviços como HAProxy, StatsD, Graphite, etc.; 
* Um alertmanager para lidar com alertas;
* Suporte a várias ferramentas. 

Muitos componentes do Prometheus são escritos em Go, tornando-os fáceis para desenvolver e implementar binarios estáticos. 

***
### Visualização com Grafana
O Grafana é uma ferramenta open source de visualização que pode ser utilizada para exibir dados de várias fontes diferentes, as mais comuns são: Graphite, InfluxDB, ElasticSearch e Prometheus.
***
Figura 2 — Grafana e suas fontes de dados (https://grafana.com/)
![alt text](Documentacao/img/Grafana_e_suas_fontes_de_dados.png "Figura 2 — Grafana e suas fontes de dados.png")
***
Com o Grafana, ao invés de escrever as queries e visualizar as métricas diretamente no Prometheus server, é possível criar dashboard para suas métricas e visualizar elas em um único lugar.

### Na prática

Agora que entendemos o que é o Prometheus e o funcionamento dos seus componentes, vamos ver um pouco dele na prática. A ideia aqui é aprender como utilizar o client de Python para exportar métricas da sua aplicação e também entender sobre as diferentes métricas. Sendo assim, toda a parte de configuração do Prometheus Server — onde os alvos de coleta e exporters são definidos — não será abordada neste post.

### Pré-requisitos
Antes de começar, é necessário instalar as ferramentas que serão utilizadas, que são o Docker, o Docker Compose e o Git.

**Dica:** No caso do Docker e Docker Compose, não instale através de instaladores de pacotes ( por exemplo o apt get). Pois há chances da versão do pacote estar desatualizada (isso já aconteceu comigo).

### Segue abaixo o link das instalações oficiais:

* Docker CE(Community Edition)
 https://docs.docker.com/install/linux/docker-ce/ubuntu/
* Docker Compose
 https://docs.docker.com/compose/install/#install-compose
* Git
 https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

***

##########
# Principais objetivos do projeto:
- [ ] Criar um docker-compouse contendo todos os containers necessarios para o monitoramento:
   ### Sao eles:
   * [x] Prometheus.()
   * [x] Grafana.()
   * [x] Cadvisor.()
   * [x] AlertManger.(Gerenciamento de alertas)
   * [x] BlacBox.()
   * [x] lertmanager-bot.(Integracao com o telegram)



########################

 ## Consultando as métricas:

### TIPO DE DADOS:


* **Scalar** - um valor de ponto flutuante numérico simples

* **Instant vector** -  um conjunto de séries temporais contendo uma única amostra para cada série temporal, todas compartilhando o mesmo carimbo de data / hora

* **Range vector** - um conjunto de séries temporais contendo um intervalo de pontos de dados ao longo do tempo para cada série temporal

* **String** - um valor de string simples; atualmente não usado

Dependendo do caso de uso (por exemplo, ao representar graficamente vs. exibir a saída de uma expressão), apenas alguns desses tipos são permitidos como resultado de uma expressão especificada pelo usuário. Por exemplo, uma expressão que retorna um vetor instantâneo é o único tipo que pode ser representado graficamente diretamente.

## Literals

**String literals** 

Strings podem ser especificados como literais entre aspas simples, aspas duplas ou crases.

O PromQL segue as mesmas regras de escape do Go . Em plicas duplas ou uma barra invertida começa uma sequência de escape, o que pode ser seguido por a, b, f, n, r, t, vou \. Caracteres específicos podem ser fornecidos usando octal ( \nnn) ou hexadecimal ( \xnn, \unnnne \Unnnnnnnn).

Nenhum escape é processado dentro de crases. Ao contrário do Go, o Prometheus não descarta novas linhas dentro dos crases.

Exemplo:
```Go
"this is a string"
'these are unescaped: \n \\ \t'
`these are not unescaped: \n ' " \t`
```

Literais flutuantes

Os valores flutuantes escalares podem ser escritos como números inteiros literais ou números de ponto flutuante no formato (espaços em branco incluídos apenas para melhor legibilidade):

```Go
[-+]?(
      [0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?
    | 0[xX][0-9a-fA-F]+
    | [nN][aA][nN]
    | [iI][nN][fF]
)
```
Exemplos:
```Go
23
-2.43
3.4e-9
0x8f
-Inf
NaN
```
## Seletores de série temporal

Seletores de vetor instantâneos

Os seletores de vetor instantâneos permitem a seleção de um conjunto de séries temporais e um único valor de amostra para cada um em um determinado carimbo de data / hora (instante): na forma mais simples, apenas um nome de métrica é especificado. Isso resulta em um vetor instantâneo contendo elementos para todas as séries temporais que possuem este nome de métrica.
***
**Este exemplo seleciona todas as séries temporais que têm o http_requests_totalnome da métrica:**

```Go
http_requests_total
```
É possível filtrar ainda mais essas séries temporais anexando uma lista separada por vírgulas de correspondências de rótulos entre chaves ( {}).

Este exemplo seleciona apenas as séries temporais com o http_requests_total nome da métrica que também têm o jobrótulo definido como prometheuse o grouprótulo definido como canary:
```GO
http_requests_total{job="prometheus",group="canary"}
```
Também é possível corresponder negativamente um valor de rótulo ou comparar valores de rótulo com expressões regulares. Existem os seguintes operadores de correspondência de rótulo:

* =: Selecione rótulos que são exatamente iguais à string fornecida.
* !=: Selecione rótulos que não sejam iguais à string fornecida.
* =~: Selecione os rótulos que correspondem ao regex com a string fornecida.
* !~: Selecione rótulos que não correspondem à expressão regular fornecida.

Por exemplo, isso seleciona todas as http_requests_totalséries de tempo para staging, testinge developmentambientes e HTTP diferentes métodos GET.
```Go
http_requests_total{environment=~"staging|testing|development",method!="GET"}
```

As correspondências de rótulo que correspondem aos valores de rótulo vazios também selecionam todas as séries temporais que não possuem o rótulo específico definido. As correspondências de Regex são totalmente ancoradas. É possível ter vários matchers para o mesmo nome de rótulo.

Os seletores de vetor devem especificar um nome ou pelo menos um marcador de correspondência que não corresponda à string vazia. A seguinte expressão é ilegal:
```
{job=~".*"} # Bad!
```
Em contraste, essas expressões são válidas porque ambas têm um seletor que não corresponde aos valores de rótulo vazios.

```Go
{job=~".+"}              # Good!
{job=~".*",method="get"} # Good!

```
Os matchers de rótulo também podem ser aplicados a nomes de métricas, combinando com o __name__rótulo interno . Por exemplo, a expressão http_requests_totalé equivalente a {__name__="http_requests_total"}. Excepto Matchers =( !=, =~, !~) pode também ser usado. A expressão a seguir seleciona todas as métricas que têm um nome começando com job::
```
{__name__=~"job:.*"}
```
O nome da métrica não deve ser uma das palavras-chave bool, on, ignoring, group_lefte group_right. A seguinte expressão é ilegal:
```
on{} # Bad!
```
Uma solução alternativa para essa restrição é usar o __name__rótulo:
```Go
{__name__="on"} # Good!
```
Todas as expressões regulares no Prometheus usam a sintaxe RE2 .

### Seletores de vetor de alcance:

Literais de vetor de intervalo funcionam como literais de vetor instantâneo, exceto que eles selecionam um intervalo de amostras de volta a partir do instante atual. Sintaticamente, uma duração de tempo é acrescentada entre colchetes ( []) no final de um seletor de vetor para especificar o quanto os valores de volta no tempo devem ser buscados para cada elemento de vetor de intervalo resultante.

Neste exemplo, selecionamos todos os valores que registramos nos últimos 5 minutos para todas as séries temporais que têm o nome da métrica http_requests_totale um jobrótulo definido para prometheus:
```Go
http_requests_total{job="prometheus"}[5m]
```
## Durações de tempo

As durações de tempo são especificadas como um número, seguido imediatamente por uma das seguintes unidades:


* ms - milissegundos
* s - segundos
* m - minutos
* h - horas
* d - dias - supondo que um dia sempre tenha 24h
* w - semanas - supondo que uma semana sempre tenha 7d
* y - anos - assumindo que um ano sempre 365d

As durações de tempo podem ser combinadas por concatenação. As unidades devem ser encomendadas da mais longa para a mais curta. Uma determinada unidade deve aparecer apenas uma vez em um período de tempo.

Aqui estão alguns exemplos de durações de tempo válidas:
```Go
5h
1h30m
5m
10s
```
### Modificador de deslocamento:

O offsetmodificador permite alterar o deslocamento de tempo para vetores instantâneos e de intervalo individuais em uma consulta.

Por exemplo, a seguinte expressão retorna o valor de http_requests_total5 minutos no passado em relação ao tempo de avaliação da consulta atual:
```Go
http_requests_total offset 5m
```
Observe que o offsetmodificador sempre precisa seguir o seletor imediatamente, ou seja, o seguinte seria correto:
```Go
sum(http_requests_total{method="GET"} offset 5m) // GOOD.
```
Embora o seguinte seja incorreto :
```Go
sum(http_requests_total{method="GET"}) offset 5m // INVALID.
```
O mesmo funciona para vetores de alcance. Isso retorna a taxa de 5 minutos que http_requests_totaltinha há uma semana:
```Go
rate(http_requests_total[5m] offset 1w)
```
***
@ modificador

O @modificador permite alterar o tempo de avaliação para vetores instantâneos e de intervalo individuais em uma consulta. O tempo fornecido para o @modificador é um carimbo de data / hora Unix e descrito com um literal float.

Por exemplo, a seguinte expressão retorna o valor de http_requests_totalat 2021-01-04T07:40:00+00:00:

```Go
http_requests_total @ 1609746000
```
Observe que o @modificador sempre precisa seguir o seletor imediatamente, ou seja, o seguinte seria correto:
```Go
sum(http_requests_total{method="GET"} @ 1609746000) // GOOD.
```
O @modificador suporta todas as representações de literais float descritos acima dentro dos limites de int64. Também pode ser usado junto com o offsetmodificador onde o deslocamento é aplicado em relação ao @ tempo do modificador, independentemente de qual modificador é escrito primeiro. Essas 2 consultas produzirão o mesmo resultado.
```Go
# offset after @
http_requests_total @ 1609746000 offset 5m
# offset before @
http_requests_total offset 5m @ 1609746000
```
Este modificador é desabilitado por padrão, uma vez que quebra a invariável que o PromQL não olha antes do tempo de avaliação para as amostras. Ele pode ser ativado definindo o --enable-feature=promql-at-modifiersinalizador. Consulte os recursos desabilitados para obter mais detalhes sobre este sinalizador.

Além disso, start()e end()também podem ser usados ​​como valores para o @modificador como valores especiais.

Para uma consulta de intervalo, eles resolvem para o início e o final da consulta de intervalo, respectivamente, e permanecem os mesmos em todas as etapas.

Para uma consulta instantânea, start()e end()ambos resolvem para o momento da avaliação.
```Go
http_requests_total @ start()
rate(http_requests_total[5m] @ end())
```
## Subconsulta
A subconsulta permite que você execute uma consulta instantânea para um determinado intervalo e resolução. O resultado de uma subconsulta é um vetor de intervalo.

Sintaxe: <instant_query> '[' <range> ':' [<resolution>] ']' [ @ <float_literal> ] [ offset <duration> ]


<resolution>é opcional. O padrão é o intervalo de avaliação global.
## Operadores
O Prometheus oferece suporte a muitos operadores binários e de agregação. Eles são descritos em detalhes na página de operadores de linguagem de expressão .

## Funções
O Prometheus suporta várias funções para operar em dados. Eles são descritos em detalhes na página de funções da linguagem de expressão .

## Comentários
PromQL suporta comentários de linha que começam com #. Exemplo:
```
    # This is a comment
```
***
## Pegadinhas
***
**Staleness**

Quando as consultas são executadas, os carimbos de data / hora nos quais os dados são amostrados são selecionados independentemente dos dados reais da série temporal. Isso serve principalmente para oferecer suporte a casos como agregação ( sum, avge assim por diante), em que várias séries temporais agregadas não se alinham exatamente no tempo. Por causa de sua independência, o Prometheus precisa atribuir um valor a esses carimbos de data / hora para cada série temporal relevante. Ele faz isso simplesmente pegando a amostra mais recente antes desse carimbo de data / hora.

Se uma verificação de destino ou avaliação de regra não retornar mais uma amostra de uma série temporal que estava presente anteriormente, essa série temporal será marcada como obsoleta. Se um destino for removido, sua série temporal retornada anteriormente será marcada como obsoleta logo em seguida.

Se uma consulta for avaliada em um registro de data e hora de amostragem após uma série temporal ser marcada como obsoleta, nenhum valor será retornado para essa série temporal. Se novas amostras forem subsequentemente ingeridas para essa série temporal, elas serão retornadas normalmente.

Se nenhuma amostra for encontrada (por padrão) 5 minutos antes de um registro de data e hora de amostragem, nenhum valor será retornado para essa série temporal neste momento. Isso efetivamente significa que as séries temporais "desaparecem" dos gráficos nos momentos em que a última amostra coletada tem mais de 5 minutos ou depois de serem marcados como obsoletos.

Staleness não será marcado para séries temporais que tenham timestamps incluídos em seus scrapes. Nesse caso, apenas o limite de 5 minutos será aplicado.

***
### Evitando consultas lentas e sobrecargas:
***


Se uma consulta precisar operar em uma grande quantidade de dados, a representação gráfica pode expirar ou sobrecarregar o servidor ou navegador. Portanto, ao construir consultas sobre dados desconhecidos, sempre comece a construir a consulta na visualização tabular do navegador de expressão de Prometheus até que o conjunto de resultados pareça razoável (centenas, não milhares, de séries temporais no máximo). Somente quando você filtrar ou agregar seus dados suficientemente, mude para o modo gráfico. Se a expressão ainda demorar muito para representar graficamente ad hoc, pré-grave-a por meio de uma regra de gravação .

Isso é especialmente relevante para a linguagem de consulta do Prometheus, em que um seletor de nome de métrica simples, como api_http_requests_totalpode se expandir para milhares de séries temporais com rótulos diferentes. Lembre-se também de que as expressões que se agregam ao longo de muitas séries temporais gerarão carga no servidor, mesmo se a saída for apenas um pequeno número de séries temporais. Isso é semelhante a como seria lento somar todos os valores de uma coluna em um banco de dados relacional, mesmo se o valor de saída fosse apenas um único número.
***
### Configuração do Prometheus
****

```yaml
# prometheus.yml

global:
  scrape_interval: 20s

# Um curto intervalo de avaliação verificará as regras de alerta com muita freqüência.
# Pode ser caro se você executar o Prometheus com mais de 100 alertas.
  evaluation_interval: 20s
  ...

rule_files:
  - 'alerts/*.yml'

scrape_configs:

```


```yaml
# alerts/example-redis.yml

groups:

- name: ExampleRedisGroup
  rules:
  - alert: ExampleRedisDown
    expr: redis_up{} == 0
    for: 2m
    labels:
      severity: critical
    annotations:
      summary: "Redis instance down"
      description: "Whatever"

```
***
## Configuração do AlertManager


```yaml
# alertmanager.yml

route:
  # Quando um novo grupo de alertas é criado por um alerta recebido, espere em
  # menos 'group_wait' para enviar a notificação inicial.
  # Desta forma, garante que você receba vários alertas para o mesmo grupo que começa
  # disparando logo após o outro são agrupados no primeiro
  # notificação.
  group_wait: 10s

 # Quando a primeira notificação foi enviada, espere 'group_interval' para enviar um lote
 # Nº de novos alertas que começaram a ser disparados para aquele grupo.
  group_interval: 30s

  # Se um alerta foi enviado com sucesso, espere 'repeat_interval' para
  # reenvie-os.
  repeat_interval: 30m


  # Um receptor padrão
  receiver: "slack"

  # Todos os atributos acima são herdados por todas as rotas secundárias e podem
  # substituído em cada um.
  routes:
    - receiver: "slack"
      group_wait: 10s
      match_re:
        severity: critical|warning
      continue: true

    - receiver: "pager"
      group_wait: 10s
      match_re:
        severity: critial
      continue: true

receivers:
  - name: "slack"
    slack_configs:
      - api_url: 'https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/xxxxxxxxxxxxxxxxxxxxxxxxxxx'
        send_resolved: true
        channel: 'monitoring'
        text: "{{ range .Alerts }}<!channel> {{ .Annotations.summary }}\n{{ .Annotations.description }}\n{{ end }}"

  - name: "pager"
    webhook_config:
      - url: http://a.b.c.d:8080/send/sms
        send_resolved: true
```
***
## Solução de problemas
***

Se a notificação demorar muito para ser acionada, verifique os seguintes atrasos:

* scrape_interval = 20s (prometheus.yml)
* evaluation_interval = 20s (prometheus.yml)
* increase(mysql_global_status_slow_queries[1m]) > 0 (alertas / exemplo-mysql.yml)
* for: 5m (alertas / exemplo-mysql.yml)
* group_wait = 10s (alertmanager.yml)
  
Leia também https://pracucci.com/prometheus-understanding-the-delays-on-alerting.html .

Alertas incríveis do Prometheus são mantidos por samber .