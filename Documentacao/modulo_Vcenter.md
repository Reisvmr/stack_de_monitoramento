## Documentação módulo Vcenter prometheus.


Para fazer esta função funcionar utilizamos a imagem docker **pryorda/vmware_exporter:v0.16.0** ela foi incluída no docker compouse do nosso projeto.

***
Abaixo irei listar todos os arquivos de configuração utilizados no projeto.
***
```yml
   vmware_exporter:
    image: pryorda/vmware_exporter:v0.16.0
    container_name: vmware_exporter
    ports:
    - 9272:9272
    dns:
    - 10.32.63.42
    - 10.32.63.46
    - 8.8.8.8
    environment:
    - VSPHERE_IGNORE_SSL=${VSPHERE_IGNORE_SSL}
    - VSPHERE_USER=${VSPHERE_USER}
    - VSPHERE_PASSWORD=${VSPHERE_PASSWORD}
```
Para preencher as variáveis utilizamos o arquivo .env na raiz do diretório.

```yml
ADMIN_USER=admin
ADMIN_PASSWORD=admin

#
#É um pre-rec que o usuário (VSPHERE_USER) exista no vcenter e #tenha permissão de #leitura em todo o ambiente.
#
#VSPHERE_IGNORE_SSL=TRUE
#VSPHERE_HOST=10.32.208.120
#VSPHERE_HOST=vcenter7.decea.intraer/
VSPHERE_IGNORE_SSL=True
VSPHERE_USER=Usuario_Vcenter
VSPHERE_PASSWORD="Senha"

```
As configuracoes de raspagem no arquivo prometheus.yml

```yml
#Localizacao do arquivo de configuracao
# ./prometheus/prometheus.yml
  - job_name: 'vmware_exporter'
    metrics_path: '/metrics'
    static_configs:
    file_sd_configs:
      - files:
        - '/etc/prometheus/targets/vcenter.yml' 
     
    relabel_configs:
    - source_labels: [__address__]
      target_label: __param_target
    - source_labels: [__param_target]
      target_label: instance
    - target_label: __address__
      replacement: vmware_exporter:9272

```
## Targets Prometheus:

```yml
#Localizacao
# ./prometheus/targets/vcenter.yml
- targets:
  - 10.32.208.120
  labels:                                                                                                                                                                               
    group: VCENTER7
- targets:
  - 10.32.209.87
  labels:                                                                                                                                                                               
    group: VCENTER6.7
  

```

