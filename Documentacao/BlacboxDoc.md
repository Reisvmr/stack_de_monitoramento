
# Como o Prometheus e o exportador de caixa preta tornam o monitoramento de endpoints de microsserviço fácil e gratuito

OBS: Esta solucao nao foi desenvolvida por mim, simplismente reutilizei um projeto ja existente seguem os links originais:
01) https://grafana.com/blog/2020/11/25/how-we-eliminated-service-outages-from-certificate-expired-by-setting-up-alerts-with-grafana-and-prometheus/
02) https://medium.com/the-telegraph-engineering/how-prometheus-and-the-blackbox-exporter-makes-monitoring-microservice-endpoints-easy-and-free-of-a986078912ee
03) https://github.com/prometheus/blackbox_exporter
***
Apresentamos uma solução simples, mas eficaz que  Monitore a data de validade dos certificados e status basicos da pagina com o Prometheus e visualize-os com o Grafana , usando recursos da nova visualização de tabelas no Grafana.

É assim que parece, todos os seus certificados em um piscar de olhos: o tempo restante até o certificado expirar, a mensagem de resposta HTTP e as métricas de conexão.
***
### Exportar e extrair as métricas

Felizmente, existe um exportador chamado blackbox que podemos usar, que oferece tudo o que precisamos para coletar esses dados, portanto, é uma implementação de “baixo código”. Só temos que cuidar da configuração e, felizmente, não temos que construir nosso próprio exportador!  

O exportador de blackbox é usado para monitorar páginas HTTP (S). Ao monitorar uma combinação [host: porta], podemos obter as informações do certificado SSL e, a partir disso, capturarmos automaticamente a data de validade e calcular o tempo restante usando a métrica `probe_ssl_earliest_cert_expiry`.   

A configuração do exportador de caixa preta é muito simples, consistindo em duas configurações:

***
01) Arquivo de configuração do Prometheus (prometheus.yml):

```yaml
- job_name: 'blackbox'
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
      - targets:
        - https://www.decea.mil.br/
        - https://webmail.decea.mil.br
        - https://aisweb.decea.mil.br/
        - https://servicos.decea.mil.br/aga/
        - https://sigma.cgna.gov.br/plni/loginPage
        - https://redemet.decea.mil.br/
        - http://tarifas.decea.mil.br/
        - http://gercob.decea.mil.br/
        - https://www.decea.mil.br/drone/
                       
        
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 192.168.1.73:9115  # Blackbox exporter scraping address
```
02) O próprio blackbox.yml : Basta configurar o módulo a seguir para exportar as métricas.

```yaml
  # O protocolo sobre o qual a investigação ocorrerá (http, tcp, dns, icmp). 
modules:
  http_2xx:
    prober: http
  http_post_2xx:
    prober: http
    http:
      method: POST
  tcp_connect:
    prober: tcp
  pop3s_banner:
    prober: tcp
    tcp:
      query_response:
      - expect: "^+OK"
      tls: true
      tls_config:
        insecure_skip_verify: true
  ssh_banner:
    prober: tcp
    tcp:
      query_response:
      - expect: "^SSH-2.0-"
  irc_banner:
    prober: tcp
    tcp:
      query_response:
      - send: "NICK prober"
      - send: "USER prober prober prober :prober"
      - expect: "PING :([^ ]+)"
        send: "PONG ${1}"
      - expect: "^:[^ ]+ 001"
  icmp:
    prober: icmp

```
***

## Sondando servidores DNS com o exportador Blackbox

Entre os tipos de sondagem do exportador Blackbox está o DNS.

Tal como acontece com outros tipos de sondagem, as sondas DNS devem ser usadas para executar uma consulta em vários servidores DNS diferentes pelo Prometheus e verificar se os servidores DNS estão funcionando. Vamos experimentar verificando se os servidores DNS estão retornando que robustperception.io está usando o Google para receber e-mails:

```yaml
# BAIXANDO PROJETO BLACKBOX
wget https://github.com/prometheus/blackbox_exporter/releases/download/v0.12.0/blackbox_exporter-0.12.0.linux-amd64.tar.gz
tar -xzf blackbox_exporter - *. linux-amd64.tar.gz
cd blackbox_exporter- *
## ARQUIVO DE CONGUFIGURACAO DO MODULO
modules:
  dns_rp_mx:
    prober: dns
    dns:
      query_name: "robustperception.io"
      query_type: "MX"
      validate_answer_rrs:
        fail_if_not_matches_regexp:
         - "robustperception.io.\t.*\tIN\tMX\t.*google.*"

```
Se você visitar  http://localhost:9115/probe?module=dns_rp_mx&target=8.8.8.8,  isso será testado no DNS público do Google e deverá funcionar. Na realidade, você geralmente aponta isso para seus próprios servidores DNS autorizados usando uma consulta de amostra em vez de verificar se um serviço DNS público está funcionando - embora você também possa usá-lo assim para verificar se registros como MX estão presentes. Para registros A e AAAA, geralmente é melhor usar uma sondagem icmp/tcp/http para ver se o serviço como um todo está funcionando, o que implicitamente também testará o DNS. Conforme mencionado em uma postagem anterior , você também pode adicionar o parâmetro debug para ver a resposta DNS completa:  http://localhost:9115/probe?module=dns_rp_mx&target=8.8.8.8&debug=true .

Você pode então usar isso em seu prometheus.yml como faria com qualquer outro módulo Blackbox:

```yaml
scrape_configs:
  - job_name: 'blackbox_dns'
    metrics_path: /probe
    params:
      module: [dns_rp_mx]
    static_configs:
      - targets:
        - 8.8.4.4  # Test various public DNS providers are working.
        - 8.8.8.8
        - 1.0.0.1
        - 1.1.1.1
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9115

```

Outros recursos do sondador de DNS incluem a capacidade de selecionar TCP ou UDP, verificar o código de resposta e verificar os registros de recursos autoritativos e adicionais que são retornados.

***
### Referencias:
* https://github.com/prometheus/blackbox_exporter/blob/master/CONFIGURATION.md#dns_probe #Documentacao Oficial
* https://github.com/prometheus/blackbox_exporter/issues/51
* https://www.robustperception.io/probing-dns-servers-with-the-blackbox-exporter
* https://prometheus.io/docs/guides/multi-target-exporter/

***
***
### Esta documentacao foi traduzida da documentacao oficial no link https://github.com/prometheus/blackbox_exporter/blob/master/CONFIGURATION.md#dns_probe , e nao é de minha autoria.
***

O arquivo é escrito no formato YAML , definido pelo esquema descrito a seguir. Os colchetes indicam que um parâmetro é opcional. Para parâmetros não listados, o valor é definido como o padrão especificado.

Os marcadores de posição genéricos são definidos da seguinte forma:

* ```<boolean>```: um booleano que pode assumir os valores trueoufalse
* ```<int>```: um número inteiro regular
* ```<duration>```: uma duração que corresponde à expressão regular [0-9]+(ms|[smhdwy])
* ```<filename>```: um caminho válido no diretório de trabalho atual
* ```<string>```: uma string regular
* ```<secret>:``` uma string regular que é um segredo, como uma senha
* ```<regex>```: uma expressão regular

Os outros marcadores de posição são especificados separadamente.


### Módulo
```yaml
  # O protocolo sobre o qual a investigação ocorrerá (http, tcp, dns, icmp). 
  prober: <prober_string>

  # Quanto tempo a sonda irá esperar antes de desistir. 
  [timeout: <duração>]

  # A configuração específica do probe - no máximo um deles deve ser especificado configuracao de sondagem. 
  [ http: <http_probe>] 
  [ tcp: <tcp_probe>] 
  [ dns: <dns_probe>] 
  [ icmp: <icmp_probe>]

```
***
## Agoara uma breve explicacao de configuracao de cada modulo:
***
# <http_probe>
```yaml
  # Códigos de status aceitos para esta sonda. O padrão é 2xx. 
  [ valid_status_codes: <int>, ... | default = 2xx] 

  # Versões HTTP aceitas para este probe. 
  [ valid_http_versions:  <string>, ...] 

  # O método HTTP que o probe usará. 
  [ method: <string> | default = "GET"] 

  # Os cabeçalhos HTTP definidos para o probe. 
  headers:
     [<string>: <string> ...] 

  # Se a detecção seguirá ou não qualquer redirecionamento. 
  [no_follow_redirects: <boolean> | default = false] 

  # Probe falha se SSL estiver presente.
  [fail_if_ssl: <boolean> | default = false] 

  # Probe falha se SSL não estiver presente. 
  [fail_if_not_ssl: <boolean> | default = false] 

  # O teste falha se o corpo da resposta corresponder ao regex. 
  fail_if_body_matches_regexp:
     [- <regex>, ...] 

  # O teste falha se o corpo da resposta não corresponder ao regex. 
  fail_if_body_not_matches_regexp:
     [- <regex>, ...] 

  # O teste falha se o cabeçalho da resposta corresponder ao regex. Para cabeçalhos com vários valores, falha se * pelo menos um * corresponder. 
  fail_if_header_matches:
     [- <http_header_match_spec>, ...] 

  #O teste falhará se o cabeçalho da resposta não corresponder ao regex. Para cabeçalhos com vários valores, falha se * nenhum * corresponder. 
  fail_if_header_not_matches:
     [- <http_header_match_spec>, ...] 

  # Configuração para o protocolo TLS do probe HTTP. 
  tls_config:
     [<tls_config>] 

  # As credenciais de autenticação básica HTTP para os destinos. 
    basic_auth:
    [ username: <string> ]
    [ password: <secret> ]
    [ password_file: <filename> ] 

  # O token do portador para os destinos. 
  [bearer_token: <secret>] 

  #O arquivo de token do portador para os destinos. 
  [bearer_token_file : <filename>] 

  # Servidor proxy HTTP a ser usado para conectar aos destinos. 
  [proxy_url: <string>] 

  # O protocolo IP da investigação HTTP (ip4, ip6). 
  [ preferred_ip_protocol: <string> | default = "ip6" ]
  [ ip_protocol_fallback: <boolean> | default = true ]

  # O corpo da solicitação HTTP usado no probe. 
   body: [ <string> ]

```
***
## <http_header_match_spec>
***

```yaml
header: <string>,
regexp: <regex>,
[ allow_missing: <boolean> | default = false ]
```
***
## <tcp_probe>
***
```yaml
# O protocolo IP da sonda TCP (ip4, ip6). 
[ preferred_ip_protocol: <string> | default = "ip6" ]
[ ip_protocol_fallback: <boolean | default = true> ]

# O endereço IP de origem. 
[ source_ip_address: <string> ]

# A consulta enviada no probe TCP e a resposta associada esperada. 
# starttls atualiza a conexão TCP para TLS. 
query_response:
  [ - [ [ expect: <string> ],
        [ send: <string> ],
        [ starttls: <boolean | default = false> ]
      ], ...
  ]

#Se o TLS é usado ou não quando a conexão é iniciada. 
[ tls: <boolean | default = false> ]

# Configuração para o protocolo TLS da sonda TCP
tls_config:
  [ <tls_config> ]
```
***
## <dns_probe>
***
```yaml
# O protocolo IP da sonda ICMP (ip4, ip6). 
[ preferred_ip_protocol: <string> | default = "ip6" ]
[ ip_protocol_fallback: <boolean | default = true> ]

# Endereco IP de Origem.
[ source_ip_address: <string> ]

[ transport_protocol: <string> | default = "udp" ] # udp, tcp

# Se deve usar DNS sobre TLS. Isso só funciona com TCP. 
[ dns_over_tls: <boolean | default = false> ]

# Configuração para protocolo TLS de DNS sobre teste TLS. 
tls_config:
  [ <tls_config> ]

query_name: <string>

[ query_type: <string> | default = "ANY" ]
[ query_class: <string> | default = "IN" ]

# Lista de códigos de resposta válidos. 
valid_rcodes:
  [ - <string> ... | default = "NOERROR" ]

validate_answer_rrs:

  fail_if_matches_regexp:
    [ - <regex>, ... ]

  fail_if_all_match_regexp:
    [ - <regex>, ... ]

  fail_if_not_matches_regexp:
    [ - <regex>, ... ]

  fail_if_none_matches_regexp:
    [ - <regex>, ... ]

validate_authority_rrs:

  fail_if_matches_regexp:
    [ - <regex>, ... ]

  fail_if_all_match_regexp:
    [ - <regex>, ... ]

  fail_if_not_matches_regexp:
    [ - <regex>, ... ]

  fail_if_none_matches_regexp:
    [ - <regex>, ... ]

validate_additional_rrs:

  fail_if_matches_regexp:
    [ - <regex>, ... ]

  fail_if_all_match_regexp:
    [ - <regex>, ... ]

  fail_if_not_matches_regexp:
    [ - <regex>, ... ]

  fail_if_none_matches_regexp:
    [ - <regex>, ... ]

```
***
## <icmp_probe>
***
```yaml
# O protocolo IP da sonda ICMP (ip4, ip6). 
[ preferred_ip_protocol: <string> | default = "ip6" ]
[ ip_protocol_fallback: <boolean | default = true> ]

# O enderco Ip de ORIGEM.
[ source_ip_address: <string> ]

# Defina o bit DF no cabeçalho IP. Funciona apenas com ip4, em sistemas * nix e 
# requer sockets raw (ou seja, root ou CAP_NET_RAW no Linux). 
[ dont_fragment: <boolean> | default = false ]

# Define o tamanho da carga util (payload)
[ payload_size: <int> ]

```
***
### Exemplo de configuracao de modulos:
Este materia foi extraido da documentacao oficial:
https://github.com/prometheus/blackbox_exporter/blob/master/example.yml
***
```yaml
modules:
  http_2xx_example:
    prober: http
    timeout: 5s
    http:
      valid_http_versions: ["HTTP/1.1", "HTTP/2.0"]
      valid_status_codes: []  # Defaults to 2xx
      method: GET
      headers:
        Host: vhost.example.com
        Accept-Language: en-US
        Origin: example.com
      no_follow_redirects: false
      fail_if_ssl: false
      fail_if_not_ssl: false
      fail_if_body_matches_regexp:
        - "Could not connect to database"
      fail_if_body_not_matches_regexp:
        - "Download the latest version here"
      fail_if_header_matches: # Verifies that no cookies are set
        - header: Set-Cookie
          allow_missing: true
          regexp: '.*'
      fail_if_header_not_matches:
        - header: Access-Control-Allow-Origin
          regexp: '(\*|example\.com)'
      tls_config:
        insecure_skip_verify: false
      preferred_ip_protocol: "ip4" # defaults to "ip6"
      ip_protocol_fallback: false  # no fallback to "ip6"
  http_post_2xx:
    prober: http
    timeout: 5s
    http:
      method: POST
      headers:
        Content-Type: application/json
      body: '{}'
  http_basic_auth_example:
    prober: http
    timeout: 5s
    http:
      method: POST
      headers:
        Host: "login.example.com"
      basic_auth:
        username: "username"
        password: "mysecret"
  http_custom_ca_example:
    prober: http
    http:
      method: GET
      tls_config:
        ca_file: "/certs/my_cert.crt"
  tls_connect:
    prober: tcp
    timeout: 5s
    tcp:
      tls: true
  tcp_connect_example:
    prober: tcp
    timeout: 5s
  imap_starttls:
    prober: tcp
    timeout: 5s
    tcp:
      query_response:
        - expect: "OK.*STARTTLS"
        - send: ". STARTTLS"
        - expect: "OK"
        - starttls: true
        - send: ". capability"
        - expect: "CAPABILITY IMAP4rev1"
  smtp_starttls:
    prober: tcp
    timeout: 5s
    tcp:
      query_response:
        - expect: "^220 ([^ ]+) ESMTP (.+)$"
        - send: "EHLO prober\r"
        - expect: "^250-STARTTLS"
        - send: "STARTTLS\r"
        - expect: "^220"
        - starttls: true
        - send: "EHLO prober\r"
        - expect: "^250-AUTH"
        - send: "QUIT\r"
  irc_banner_example:
    prober: tcp
    timeout: 5s
    tcp:
      query_response:
        - send: "NICK prober"
        - send: "USER prober prober prober :prober"
        - expect: "PING :([^ ]+)"
          send: "PONG ${1}"
        - expect: "^:[^ ]+ 001"
  icmp_example:
    prober: icmp
    timeout: 5s
    icmp:
      preferred_ip_protocol: "ip4"
      source_ip_address: "127.0.0.1"
  dns_udp_example:
    prober: dns
    timeout: 5s
    dns:
      query_name: "www.prometheus.io"
      query_type: "A"
      valid_rcodes:
      - NOERROR
      validate_answer_rrs:
        fail_if_matches_regexp:
        - ".*127.0.0.1"
        fail_if_all_match_regexp:
        - ".*127.0.0.1"
        fail_if_not_matches_regexp:
        - "www.prometheus.io.\t300\tIN\tA\t127.0.0.1"
        fail_if_none_matches_regexp:
        - "127.0.0.1"
      validate_authority_rrs:
        fail_if_matches_regexp:
        - ".*127.0.0.1"
      validate_additional_rrs:
        fail_if_matches_regexp:
        - ".*127.0.0.1"
  dns_soa:
    prober: dns
    dns:
      query_name: "prometheus.io"
      query_type: "SOA"
  dns_tcp_example:
    prober: dns
    dns:
      transport_protocol: "tcp" # defaults to "udp"
      preferred_ip_protocol: "ip4" # defaults to "ip6"
      query_name: "www.prometheus.io"
```