


### Conceitos e definiçoes:
### Introdução:
HAProxy, que significa High Availability Proxy, é um popular balanceador de carga de TCP / HTTP e solução de proxy que pode ser executado em Linux, Solaris e FreeBSD. Seu uso mais comum é melhorar o desempenho e a confiabilidade de um ambiente de servidor, distribuindo a carga de trabalho em vários servidores (por exemplo, web, aplicativo, banco de dados). Ele é usado em muitos ambientes de alto perfil, incluindo: GitHub, Imgur, Instagram e Twitter.

Neste guia, forneceremos uma visão geral do que é o HAProxy, terminologia básica de balanceamento de carga e exemplos de como ele pode ser usado para melhorar o desempenho e a confiabilidade de seu próprio ambiente de servidor.

## Terminologia HAProxy
Existem muitos termos e conceitos importantes ao discutir o balanceamento de carga e o proxy. Examinaremos os termos comumente usados ​​nas subseções a seguir.

Antes de entrarmos nos tipos básicos de balanceamento de carga, falaremos sobre **ACLs**, **back-ends** e **front-ends**.
## Lista de controle de acesso (ACL)
Em relação ao balanceamento de carga, as ACLs são usadas para testar algumas condições e executar uma ação (por exemplo, selecionar um servidor ou bloquear uma solicitação) com base no resultado do teste. O uso de ACLs permite o encaminhamento flexível do tráfego de rede com base em uma variedade de fatores, como correspondência de padrões e o número de conexões com um back-end, por exemplo.

Exemplo de uma ACL:
```
acl url_blog path_beg /blog
```
Essa ACL será correspondida se o caminho da solicitação de um usuário começar com / blog . Isso corresponderia a uma solicitação de http://yourdomain.com/blog/blog-entry-1 , por exemplo.


Para obter um guia detalhado sobre o uso de ACL, consulte o Manual de configuração do HAProxy
https://cbonte.github.io/haproxy-dconv/2.0/configuration.html#7

### Backend


É um conjunto de servidores que recebe solicitações encaminhadas. Os back- ends são definidos na seção de "back-end" da configuração do HAProxy. 
Em sua forma mais básica, um back-end pode ser definido por:

* Qual algoritmo de equilíbrio de carga usar. 
* Em uma determinada lista de servidores e portas.

Um back-end pode conter um ou vários servidores 
- falando de modo geral, adicionar mais servidores ao back-end aumentará sua capacidade de carga potencial ao distribuir a carga por vários servidores.
-  O aumento da confiabilidade também é obtido dessa maneira, caso alguns de seus servidores back-end fiquem indisponíveis.

Aqui está um exemplo de uma configuração de dois backend, web-backend e blogue-backend com dois servidores web em cada um, escutando na porta 80:

```
backend web-backend
   balance roundrobin
   server web1 web1.yourdomain.com:80 check
   server web2 web2.yourdomain.com:80 check

backend blog-backend
   balance roundrobin
   mode http
   server blog1 blog1.yourdomain.com:80 check
   server blog1 blog1.yourdomain.com:80 check
```
balance roundrobinlinha especifica o algoritmo de balanceamento de carga, que é detalhado na seção Algoritmos de balanceamento de carga 
- https://www.digitalocean.com/community/tutorials/an-introduction-to-haproxy-and-load-balancing-concepts#load-balancing-algorithms.

mode httpespecifica que o proxy da camada 7 será usado, o que é explicado na seção Tipos de balanceamento de carga .

A checkopção no final das serverdiretivas especifica que as verificações de integridade devem ser realizadas nesses servidores back-end.

## Frontend 
Um front-end define como as solicitações devem ser encaminhadas aos back-ends. 
Os front-ends são definidos na seção de front-end da configuração do HAProxy. 

**Suas definições são compostas dos seguintes componentes:**

* Um conjunto de endereços IP e uma porta (por exemplo, 10.1.1.7:80, *: 443, etc.)
* ACLs
* use_backend rules, definem quais back-ends usar, dependendo de quais condições ACL são correspondidas and/or uma regra default_backend que lida com todos os outros casos
  
Um front-end pode ser configurado para vários tipos de tráfego de rede, conforme explicado na próxima seção.
## Tipos de balanceamento de carga
Agora que entendemos os componentes básicos usados ​​no balanceamento de carga, vamos entrar nos tipos básicos de balanceamento de carga.
**Sem balanceamento de carga**
Um ambiente de aplicativo da web simples sem balanceamento de carga pode ter a seguinte aparência

![alt text](haproxy/img/web_server.png "Sem balanceamento de carga")

Neste exemplo, o usuário se conecta diretamente ao seu servidor web, em seudominio.com e não há balanceamento de carga. Se o seu único servidor web cair, o usuário não poderá mais acessar o seu servidor web. 

Além disso, se muitos usuários estão tentando acessar o servidor simultaneamente e ele não consegue lidar com a carga, eles podem ter uma experiência lenta ou podem não conseguir se conectar.

**Balanceamento de carga da camada 4:**
A maneira mais simples de balancear a carga do tráfego de rede para vários servidores é usar o balanceamento de carga da camada 4 (camada de transporte).

O balanceamento de carga dessa forma encaminhará o tráfego do usuário com base no intervalo de IP e na porta (ou seja, se uma solicitação chegar a http://seudominio.com/qualquer coisa , o tráfego será encaminhado para o backend que lida com todas as solicitações para seudominio.com em porta 80 ). Para obter mais detalhes sobre a camada 4, verifique a subseção TCP de nossa Introdução à Rede .

Aqui está um diagrama de um exemplo simples de balanceamento de carga da camada 4:

![alt text](haproxy/img/layer_4_load_balancing.png "balanceamento de carga da camada 4")

O usuário acessa o balanceador de carga, que encaminha a solicitação do usuário ao grupo de back -end da web de servidores de back-end . Qualquer que seja o servidor de back-end selecionado, ele responderá diretamente à solicitação do usuário. Geralmente, todos os servidores no back-end da web devem servir conteúdo idêntico - caso contrário, o usuário pode receber conteúdo inconsistente. Observe que os dois servidores da web se conectam ao mesmo servidor de banco de dados.
## Balanceamento de carga da camada 7:
Outra maneira mais complexa de balancear a carga do tráfego de rede é usar o balanceamento de carga da camada 7 (camada de aplicativo). O uso da camada 7 permite que o balanceador de carga encaminhe solicitações para diferentes servidores de back-end com base no conteúdo da solicitação do usuário. Este modo de balanceamento de carga permite que você execute vários servidores de aplicativos da web no mesmo domínio e porta. Para obter mais detalhes sobre a camada 7, verifique a subseção HTTP de nossa Introdução à Rede .

Aqui está um diagrama de um exemplo simples de balanceamento de carga da camada 7:
![alt text](haproxy/img/layer_7_load_balancing.png "balanceamento de carga da camada 4")

### Documentação da configuração utilizada no ha proxy:

Neste exemplo, se um usuário solicitar seudominio.com/blog , ele será encaminhado para o back-end do blog , que é um conjunto de servidores que executam um aplicativo de blog. Outras solicitações são encaminhadas para o back-end da web , que pode estar executando outro aplicativo. Ambos os back-ends usam o mesmo servidor de banco de dados, neste exemplo.

Um snippet do exemplo de configuração de front-end ficaria assim:
```
frontend http
  bind *:80
  mode http

  acl url_blog path_beg /blog
  use_backend blog-backend if url_blog

  default_backend web-backend
```
Isso configura um front-end denominado http , que lida com todo o tráfego de entrada na porta 80.

**acl url_blog** path_beg /blogcorresponde a uma solicitação se o caminho da solicitação do usuário começar com / blog .

**use_backend** "blog-backend" if url_blogusa a ACL para fazer proxy do tráfego para o back-end do blog .

**default_backend** web-backendespecifica que todo o outro tráfego será encaminhado para o back-end da web .

### Load Balancing Algorithms:

O algoritmo de balanceamento de carga usado determina qual servidor, em um back-end, será selecionado durante o balanceamento de carga. O HAProxy oferece várias opções de algoritmos. Além do algoritmo de balanceamento de carga, os servidores podem ser atribuídos a um parâmetro de peso para manipular a freqüência com que o servidor é selecionado, em comparação com outros servidores.

Como o HAProxy fornece muitos algoritmos de balanceamento de carga, descreveremos apenas alguns deles aqui. Consulte o Manual de configuração do HAProxy para obter uma lista completa de algoritmos.

Alguns dos algoritmos comumente usados ​​são os seguintes:

### roundrobin
Round Robin seleciona servidores em turnos. Este é o algoritmo padrão.

### leastconn
Seleciona o servidor com o menor número de conexões - é recomendado para sessões mais longas. Os servidores no mesmo back-end também são girados em rodízio.

### source
Isso seleciona qual servidor usar com base em um hash do IP de origem, ou seja, o endereço IP do seu usuário. Este é um método para garantir que um usuário se conecte ao mesmo servidor.

### Sticky Sessions
Alguns aplicativos exigem que o usuário continue a se conectar ao mesmo servidor back-end. Essa persistência é obtida por meio de sessões persistentes, usando o parâmetro appsession no back-end que o requer.

### Health Check
O HAProxy usa verificações de integridade para determinar se um servidor de back-end está disponível para processar solicitações. Isso evita a necessidade de remover manualmente um servidor do back-end se ele ficar indisponível. A verificação de saúde padrão é tentar estabelecer uma conexão TCP com o servidor, ou seja, verifica se o servidor backend está escutando no endereço IP e na porta configurados.

Se um servidor falhar em uma verificação de integridade e, portanto, não puder atender às solicitações, ele será automaticamente desabilitado no back-end, ou seja, o tráfego não será encaminhado para ele até que se torne saudável novamente. Se todos os servidores em um back-end falharem, o serviço ficará indisponível até que pelo menos um desses servidores de back-end esteja íntegro novamente.

Para certos tipos de back-ends, como servidores de banco de dados em certas situações, a verificação de integridade padrão é insuficiente para determinar se um servidor ainda está íntegro.


### Other Solutions
Se você acha que o HAProxy pode ser muito complexo para as suas necessidades, as seguintes soluções podem ser mais adequadas:

* **Linux Virtual Servers (LVS)** - um balanceador de carga simples e rápido de camada 4 incluído em muitas distribuições Linux

* **Nginx** - um servidor da web rápido e confiável que também pode ser usado para fins de proxy e balanceamento de carga. Nginx é frequentemente usado em conjunto com o HAProxy para seus recursos de armazenamento em cache e compactação

### High Availability
As configurações de balanceamento de carga das camadas 4 e 7 descritas antes usam um balanceador de carga para direcionar o tráfego para um dos muitos servidores de back-end. No entanto, seu balanceador de carga é um ponto único de falha nessas configurações; se ele cair ou ficar sobrecarregado com solicitações, pode causar alta latência ou tempo de inatividade para o seu serviço.

Uma configuração de alta disponibilidade (HA) é uma infraestrutura sem um único ponto de falha. Ele evita que uma única falha de servidor seja um evento de tempo de inatividade, adicionando redundância a todas as camadas de sua arquitetura. Um balanceador de carga facilita a redundância para a camada de back-end (servidores web / app), mas para uma configuração de alta disponibilidade real, você também precisa ter balanceadores de carga redundantes.

Aqui está um diagrama de uma configuração básica de alta disponibilidade:
![alt text](haproxy/img/ha-diagram-animated.gif )
***
Neste exemplo, você tem vários balanceadores de carga (um ativo e um ou mais passivos) por trás de um endereço IP estático que pode ser remapeado de um servidor para outro. Quando um usuário acessa seu site, a solicitação passa pelo endereço IP externo para o balanceador de carga ativo. Se esse balanceador de carga falhar, seu mecanismo de failover irá detectá-lo e reatribuir automaticamente o endereço IP a um dos servidores passivos. Existem várias maneiras diferentes de implementar uma configuração de HA ativa / passiva. Para saber mais, leia esta seção de Como usar IPs flutuantes .

### defaults

**log global:**
Numero maximo de conexoes por processo.

**option dontlognull:** Não ira logar requests nulas.

Existem grandes sites que lidam com vários milhares de conexões por segundo e para o qual o registro é uma grande dor de cabeça. Alguns delesão até  forçados a tirar o serviço do ar e não pode depurar problemas de produção. 
Definir esta opção garante que conexões normais serão logadas, aquelas que não apresentam nenhum erro, nenhum tempo limite, nenhuma novtentativa nem redispatch, não será registrado. Isso deixa espaço em disco para anomalias.
Em HTTP modo, o código de status de resposta é verificado e os códigos de retorno 5xx ainda serão
registrado. 
É altamente desencorajado usar esta opção, pois na maioria das 
vezes, a chave para problemas complexos estão nos logs norma 
que não serão registrados aqui. Se você precisa separar os log 
consulte a opção " log-separar-erros ".


### Timeout Confs:

**timeout queue:**
Tempo de espera na fila em caso de estouro do maximo de conexoes aceitas.
 
**timeout connect:**
Tempo limite de espera para o haproxy conectar no backend server.
**timeout connect: or timeout server:**
Tempo limete de cociosidade do client ou do servidor para uma conexao.
**timeout check:**
Tempo limite de checagem dos backends
**maxconn:**
Numero maximo de conexoes por usuário
***

### 